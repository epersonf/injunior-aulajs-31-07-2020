//#region Questao 1

/*
    Criar uma função que, dado um número indefinido de arrays,
    faça a soma de seus valores
    Ex: [1,2,3] [1,2,2] [1,1] => 13
        [1,1] [2, 20] => 24
*/

let input = [1,2,3] [1,2,2] [1,1];

function SumAllArrays(...num) {
    let sum = 0;
    num.forEach((e) => {
        e.forEach(element => {
            sum += element;
        });
    });
    return sum;
}

//console.log(SumAllArrays([1,2,3], [1,2,2], [1,1]));

//#endregion

//#region Questao 2

/*
    Criar uma função que dado um número n e um array, retorne
    um novo array com os valores do array anterior * n
    Ex: (2, [1,3,6,10]) => [2,6,12,20]
        (3, [7,9,11,-2]) => [21, 27, 33, -6]
*/

function DuplicateArray(n, arr) {
    let toReturn = arr.map(e => {
        return e * n;
    });
    return toReturn;
}

//console.log(DuplicateArray(2, [1, 3, 6, 10]));

//#endregion

//#region Questao 3

/*
    Crie uma função que dado uma string A e um array de strings,
    retorne um array novo com apenas as strings do array que são
    compostas exclusivamente por caracteres da string A

    Ex: ("ab", ["abc", "ba", "ab", "bb", "kb"]) => ["ba", "ab", "bb"]
        ("pkt", ["pkt", "pp", "pata", "po", "kkkkkkk"]) => ["pkt", pp, kkkkkkk]
*/

function ArrayOfStrings(startString, arrayString) {
    let toReturn = [];
    for (let j = 0; j < arrayString.length; j++) {
        for (let i = 0; i < arrayString[j].length; i++) {        
            if (!startString.includes(arrayString[j][i])) break;
            if (i != arrayString[j].length - 1) continue;
            toReturn.push(arrayString[j]);
        }
    }
    return toReturn;
}

//console.log(ArrayOfStrings("ab", ["abc", "ba", "ab", "bb", "kb"]));

//#endregion

//#region Questao 4

/*
    Criar uma função que dado n arrays, retorne um novo array que possua
    apenas os valores que existem em todos os n arrays
    Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]
        [120, 120, 110, 2] [110, 2, 130] => [110, 2]
*/


function FindCommonNumber(...arrayList) {
    let toReturn = [];

    for (let j = 0; j < arrayList[0].length; j++) {

        for (let i = 1; i < arrayList.length; i++) {
            if (!arrayList[i].includes(arrayList[0][j])) break;
            
            if (!toReturn.includes(arrayList[0][j]))
                toReturn.push(arrayList[0][j]);
        }
    }

    return toReturn;

}


//console.log(FindCommonNumber([1, 2, 3], [3, 3, 7], [9, 111, 3]));


//#endregion

//#region Questao 5
/*
    Crie uma função que dado n arrays, retorne apenas os que tenham a
    soma de seus elementos par
    Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]
        [2,2,2,1] [3, 2, 1] => [3,2,1] 
*/

function ReturnEvenArrays(...arrayList) {

    let toReturn = arrayList.filter(arrayOfNumbers => {
        
        let sum = 0;
        arrayOfNumbers.forEach(number => {
            sum += number;
        });

        if (sum % 2 == 0) return arrayOfNumbers;
    });

    return toReturn;

}

console.log(ReturnEvenArrays([1, 1, 3], [1, 2, 2, 2, 3], [2]));

//#endregion